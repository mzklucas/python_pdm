## Importar bibliotecas
## tkinter - biblioteca grafica
## Nota    - classe de dados
## sqlite3 - biblioteca de acesso a sql

import tkinter
import Nota
import sqlite3

## O programa guarda notas na memoria, possuindo quatro acoes: 
## mostrar(): mostra todas as em memória
## criar(): salva nota escrita na caixa de texto (entry)
## salvar(): salva as notas da memória em um arquivo
## carregar(): carrega as notas do arquivo

def mostrar():
    global mensagens
    for i in mensagens:
        nova_janela = tkinter.Tk()
        nova_janela.geometry("200x200")
        nova_mensagem = tkinter.Label(nova_janela, text = i.texto)
        nova_mensagem.pack()

def criar():
    global mensagens
    nova_nota = Nota.Nota(len(mensagens), entrada_de_texto.get())
    mensagens.append(nova_nota)
    entrada_de_texto.delete(0, tkinter.END)

## Nas funcoes de salvar e carregar ha uma engembra. As notas sao escritas
## linha a linha, sendo a sua linha o seu ID. Para carregar as notas, usamos
## a funcao readlines, que carrega cada linha como uma string de uma lista

def salvar():
    global mensagens
    arquivo = open("mensagens.txt", 'w')
    for i in mensagens:
        arquivo.write(i.texto + "\n")
    arquivo.close()

def carregar():
    global mensagens    
    arquivo = open("mensagens.txt", 'r')
    saida = arquivo.readlines()
    mensagens = []
    for i in saida:
        mensagens.append (Nota.Nota(len(mensagens), i))
    arquivo.close()

## Area inicial do programa.
## Guardamos as notas em uma variavel global "mensagens", uma lista
## que contem objetos do tipo Nota, com uma id e um texto

if __name__ == "__main__":

    global mensagens
    mensagens = []

    ## criar janela
    
    janela = tkinter.Tk()
    janela.geometry ("500x200")

    ## criar elementos da janela

    botao_mostrar = tkinter.Button(janela, text = "Mostre as Mensagens", command = mostrar)
    botao_criar = tkinter.Button(janela, text = "Criar", command = criar)
    botao_salvar = tkinter.Button(janela, text = "Salvar", command = salvar)
    botao_carregar = tkinter.Button(janela, text = "Carregar", command = carregar)
    entrada_de_texto = tkinter.Entry(janela)

    ## Empacotar os elementos

    botao_mostrar.pack()
    entrada_de_texto.pack()
    botao_criar.pack()
    botao_salvar.pack()
    botao_carregar.pack()
    
    janela.mainloop()
